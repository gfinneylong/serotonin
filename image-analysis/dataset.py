__author__ = 'gio'
import glob
import os


class Dataset:
    """
    A dataset is used to describe a set of files which constitute a scan,
    as well as all files generated from processing these files
    """
    all = dict()  # All datasets are loaded in, and indexed by key

    def __init__(self, directory, file_pattern, key, picklefile=None, channel=3, network_drive=False, note=''):
        self.directory = os.path.join(os.path.normpath(os.path.expanduser(directory)), '')
        self.file_pattern = file_pattern
        if picklefile is not None:
            self.picklefile = picklefile
            if picklefile[-7] != '.pickle':
                self.picklefile += '.pickle'
        else:
            self.picklefile = str(key) + '.pickle'
        self.key = key
        self.channel = channel
        if key in Dataset.all:
            raise Exception("Cannot add key %s to Datasets; key already in use: %s" % (key, Dataset.all[key]))
        self.files = get_images(self.directory, self.file_pattern)
        self.note = note
        Dataset.all[self.key] = self

    def __str__(self):
        ret = "Dataset:(key:%s, directory:%s, picklefile:%s" % (self.key, self.directory, self.picklefile)
        if self.note != '':
            ret += ', %s)' % self.note
        else:
            ret += ')'
        return ret

    __repr__ = __str__


def get_images(directory, file_name_pattern):
    """
    Gets filename of images which are to be processed, based on configuration in myconfig.py
    :return: A list of filenames which contain images
    """
    directory = os.path.join(directory, '')  # Add trailing slash
    all_images = glob.glob(directory + file_name_pattern)
    if len(all_images) == 0:
        raise Exception('No images found matching the pattern: ' + str(directory + file_name_pattern))
    return all_images


def make_datasets():
    """
    THIS MUST BE FILLED IN WITH ONE ENTRY FOR EACH DATASET!!
    :return:
    """
    # ********************************************************************
    # MUST FILL THIS IN TO DEFINE DATASETS WHICH CAN THEN BE CHOSEN BY KEY
    # ********************************************************************

    # Example:
    # Dataset(
    #     'C:/Path/To/Dataset/Folder',
    #     '*.dataset_extension', # Commonly .tif (would be *.tif)
    #     'M8', # A memorable key to reference the dataset by. The dataset to reference is assigned in myconfig's Config.dataset_key
    #     'M8_C57BL6_Adult_Saline', # The file to save the processed dataset as, '.pickle' extension not required
    #     channel=3,
    #     network_drive=False,
    #     note='Fluoxetine' # Additional info about the dataset which one may wish to remember
    # )


    # NOTE: Keep this line at the end, after the dataset definitions.
    if len(Dataset.all) == 0:
        raise Exception("Chose to use datasets, but configuration unset.\nConfigure one or more datasets within make_datasets()")
